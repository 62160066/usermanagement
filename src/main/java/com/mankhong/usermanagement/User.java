/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mankhong.usermanagement;

import java.io.Serializable;

/**
 *
 * @author W.Home
 */
public class User implements Serializable {
    
    private String userName;
    private String UserPassword;
    
    public User(String userName , String password){
        this.userName = userName;
        this.UserPassword = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return UserPassword;
    }

    public void setUserPassword(String UserPassword) {
        this.UserPassword = UserPassword;
    }
    public String toString(){
        return "{ Username = "+userName+" , password = "+UserPassword+" }"; 
    }
    
    
}
